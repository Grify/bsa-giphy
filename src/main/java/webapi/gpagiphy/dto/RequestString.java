package webapi.gpagiphy.dto;

import lombok.Data;

@Data
public class RequestString {
    private String query;
}
