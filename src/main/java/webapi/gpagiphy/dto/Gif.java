package webapi.gpagiphy.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Gif {
    private String type;
    private String id;
    private String url;
    private String title;
}
