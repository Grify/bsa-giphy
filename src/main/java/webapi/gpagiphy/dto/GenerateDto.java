package webapi.gpagiphy.dto;

import lombok.Data;

@Data
public class GenerateDto {
    private String query;
    private Boolean force;
}
