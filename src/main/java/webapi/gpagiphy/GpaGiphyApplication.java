package webapi.gpagiphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GpaGiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GpaGiphyApplication.class, args);
    }

}
