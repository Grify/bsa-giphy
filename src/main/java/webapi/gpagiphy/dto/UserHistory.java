package webapi.gpagiphy.dto;

import lombok.Data;

@Data
public class UserHistory {
    private String date;
    private String query;
    private String gif;
}
