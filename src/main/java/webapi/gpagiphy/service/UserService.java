package webapi.gpagiphy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import webapi.gpagiphy.dto.GenerateDto;
import webapi.gpagiphy.dto.GifsByQuery;
import webapi.gpagiphy.dto.GiphyResponse;
import webapi.gpagiphy.dto.UserHistory;

import java.io.*;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

@Service
public class UserService {
    @Autowired
    CacheService cacheService;
    @Value(value = "${giphy.api-key}")
    private String API_KEY;
    @Value(value = "${giphy.get-data}")
    private String PATH_TO_GIPHY;
    @Value(value = "${giphy.download-ulr}")
    private String PATH_TO_DOWNLOAD;


    public List<GifsByQuery> getAllById(String id) {
        new File("./src/main/resources/users").mkdir();
        File catalog = new File("./src/main/resources/users/" + id);
        List<GifsByQuery> response = new ArrayList<>();
        if (catalog.listFiles() == null) {
            return response;
        }
        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            if (dir.isDirectory()) {
                ArrayList<String> fileNames = new ArrayList<>();
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    fileNames.add(file.getAbsolutePath());
                }
                GifsByQuery gifs = new GifsByQuery();
                gifs.setQuery(dir.getName());
                gifs.setGifs(fileNames);

                response.add(gifs);
            }
        }
        return response;
    }

    public List<UserHistory> getHistoryById(String id) {
        new File("./src/main/resources/users").mkdir();
        File catalog = new File("./src/main/resources/users/" + id);
        List<UserHistory> response = new ArrayList<>();

        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            if (dir.isFile() && dir.getName().equals("history.csv")) {
                try {
                    FileReader fr = new FileReader(dir);
                    BufferedReader reader = new BufferedReader(fr);
                    String line = reader.readLine();
                    while (line != null) {
                        UserHistory action = new UserHistory();
                        String[] data = line.split(",");
                        action.setDate(data[0]);
                        action.setQuery(data[1]);
                        action.setGif(data[2]);
                        response.add(action);

                        line = reader.readLine();
                    }
                    fr.close();
                    reader.close();
                    return response;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response;
    }

    public void cleanHistory(String id) {
        new File("./src/main/resources/users").mkdir();
        File catalog = new File("./src/main/resources/users/" + id);
        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            if (dir.isFile() && dir.getName().equals("history.csv")) {
                try {
                    if (dir.delete()) {
                        dir.createNewFile();
                        return;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String find(String id, String query, Boolean force) {
        new File("./src/main/resources/users").mkdir();
        String response = "";
        if (!force) {
            List<GifsByQuery> gifs = cacheService.getGifsByQuery(query);
            if (gifs.size() > 0) {
                response = gifs.get(0).getGifs().get(0);
            }
        }
        if (!response.equals("")) {
            return response;
        }

        File catalog = new File("./src/main/resources/users/" + id);
        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            if (dir.isDirectory() && dir.getName().equals(query)) {
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    response = file.getAbsolutePath();
                    try {
                        new File("./src/main/resources/cache/" + query).mkdir();
                        Path copied = Paths.get("./src/main/resources/cache/" + query + "/" + file.getName());
                        Path originalPath = Paths.get(response);
                        Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    break;
                }
            }
        }
        if (response.equals("")) {
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Gif not found."
            );
        }
        return response;
    }

    public void resetCache(String id, String query) {
        new File("./src/main/resources/users").mkdir();
        new File("./src/main/resources/cache").mkdir();
        File catalogCache = new File("./src/main/resources/cache");
        for (File dir : Objects.requireNonNull(catalogCache.listFiles())) {
            if (dir.getName().equals(query) || query.equals("")) {
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    file.delete();
                }
                dir.delete();
            }
        }
    }

    public void cleanAll(String id) {
        new File("./src/main/resources/users").mkdir();
        File catalogCache = new File("./src/main/resources/cache");
        for (File dir : Objects.requireNonNull(catalogCache.listFiles())) {
            for (File file : Objects.requireNonNull(dir.listFiles())) {
                file.delete();
            }
            dir.delete();
        }

        File catalogUsers = new File("./src/main/resources/users/" + id);
        for (File dir : Objects.requireNonNull(catalogUsers.listFiles())) {
            if (dir.isDirectory()) {
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    file.delete();
                }
            }
            dir.delete();
        }
        catalogUsers.delete();
    }

    public String create(String id, GenerateDto request) {
        new File("./src/main/resources/users").mkdir();
        String pathTo = "";
        if (!request.getForce()) {
            List<GifsByQuery> gifs = cacheService.getGifsByQuery(request.getQuery());
            if (gifs.size() > 0) {
                pathTo = gifs.get(0).getGifs().get(0);
            }
        }
        if (!pathTo.equals("")) {
            try {
                FileWriter fw = new FileWriter("./src/main/resources/users/" + id + "/history.csv", true);
                Calendar date = Calendar.getInstance();
                fw.write( date.get(Calendar.DAY_OF_MONTH) + "-" + date.get(Calendar.MONTH) + "-" +
                        date.get(Calendar.YEAR) +"," + request.getQuery() + "," + pathTo + "\n");
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return pathTo;
        }

        // Download
        RestTemplate restTemplate = new RestTemplate();
        GiphyResponse giphyResponse = restTemplate.getForObject(
                PATH_TO_GIPHY + "?api_key=" + API_KEY + "&q=" + request.getQuery(), GiphyResponse.class);
        if (giphyResponse == null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_GATEWAY, "Giphy does not work."
            );
        }
        String fileURL = PATH_TO_DOWNLOAD + giphyResponse.getData().get(0).getId() + ".gif";

        try {
            URL url = new URL(fileURL);
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            new File("./src/main/resources/users/" + id).mkdir();
            new File("./src/main/resources/users/" + id + "/" + request.getQuery()).mkdir();
            new File("./src/main/resources/cache/" + request.getQuery()).mkdir();
            pathTo = "./src/main/resources/users/" + id +
                    "/" + request.getQuery() + "/" + giphyResponse.getData().get(0).getId() + ".gif";
            FileOutputStream fosUser = new FileOutputStream(pathTo);
            fosUser.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fosUser.close();
            rbc.close();

            new File("./src/main/resources/cache/" + request.getQuery()).mkdir();
            Path copied = Paths.get("./src/main/resources/cache/" + request.getQuery()
                    + "/" + giphyResponse.getData().get(0).getId() + ".gif");
            Path originalPath = Paths.get(pathTo);
            Files.copy(originalPath, copied, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e ) {
            e.printStackTrace();
        }

        try {
            FileWriter fw = new FileWriter("./src/main/resources/users/" + id + "/history.csv", true);
            Calendar date = Calendar.getInstance();
            fw.write( date.get(Calendar.DAY_OF_MONTH) + "-" + (date.get(Calendar.MONTH) + 1) + "-" +
                    date.get(Calendar.YEAR) +"," + request.getQuery() + "," + pathTo + "\n");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return pathTo;
    }
}
