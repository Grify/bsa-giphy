package webapi.gpagiphy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import webapi.gpagiphy.dto.GifsByQuery;
import webapi.gpagiphy.dto.RequestString;
import webapi.gpagiphy.service.CacheService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@RestController
public class CacheController {
    @Autowired
    private CacheService cacheService;
    Logger logger = LoggerFactory.getLogger(CacheController.class);

    @GetMapping("/cache")
    public List<GifsByQuery> getByQuery(@RequestParam(name = "query", required = false, defaultValue = "") String query) {
        logger.info("Path: /cache. Method: GET. Query:\"" + query + "\"");
        if (Pattern.matches("[a-zA-Z0-9]*", query)) {
            return cacheService.getGifsByQuery(query);
        } else {
            return new ArrayList<>();
        }
    }

    @PostMapping("/cache/generate")
    public GifsByQuery create(@RequestHeader("accept-language") String language,
                              @RequestBody RequestString query) {
        if (!language.equals("en-US")) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Bad HEADER."
            );
        }
        logger.info("Path: /cache/generate. Method: POST. Query:\"" + query + "\"");
        if (Pattern.matches("[a-zA-Z0-9]*", query.getQuery())) {
            return cacheService.create(query.getQuery());
        } else {
            return new GifsByQuery();
        }

    }

    @DeleteMapping("/cache")
    public void delete(@RequestHeader("accept-language") String language) {
        if (!language.equals("en-US")) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Bad HEADER."
            );
        }
        logger.info("Path: /cache/generate. Method: DELETE.");
        cacheService.delete();
    }

    @GetMapping("/gifs")
    public List<String> getAll() {
        logger.info("Path: /gifs. Method: GET.");
        return cacheService.getAllGifs();
    }
}
