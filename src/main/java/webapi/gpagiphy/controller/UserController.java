package webapi.gpagiphy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import webapi.gpagiphy.dto.GenerateDto;
import webapi.gpagiphy.dto.GifsByQuery;
import webapi.gpagiphy.dto.UserHistory;
import webapi.gpagiphy.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    UserService userService;
    Logger logger = LoggerFactory.getLogger(CacheController.class);

    @GetMapping("/{id}/all")
    public List<GifsByQuery> getAllByUser(@PathVariable(name = "id") String id) {
        logger.info("Path: /user/{id}/all. Method: GET. Id: \"" + id + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id)) {
            return userService.getAllById(id);
        } else {
            return new ArrayList<>();
        }
    }

    @GetMapping("/{id}/history")
    public List<UserHistory> getHistory(@PathVariable(name = "id") String id) {
        logger.info("Path: /user/{id}/history. Method: GET. Id: \"" + id + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id)) {
            return userService.getHistoryById(id);
        } else {
            return new ArrayList<>();
        }
    }

    @DeleteMapping("/{id}/history/clean")
    public void deleteHistory(@PathVariable(name = "id") String id) {
        logger.info("Path: /user/{id}/history/clean. Method: DELETE. Id: \"" + id + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id)) {
            userService.cleanHistory(id);
        }
    }

    @GetMapping("/{id}/search")
    public String findByIdAndQuery(@PathVariable(name = "id") String id,
                                   @RequestParam(name = "query") String query,
                                   @RequestParam(name = "force", required = false, defaultValue = "false") Boolean force) {
        logger.info("Path: /user/{id}/search. Method: GET. Id: \"" + id + "\". Query: \"" + query + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id) &&
            Pattern.matches("[a-zA-Z0-9]*", query)) {
            return userService.find(id, query, force);
        } else {
            return "";
        }
    }

    @PostMapping("/{id}/generate")
    public String createById(@PathVariable(name = "id") String id,
                             @RequestHeader("accept-language") String language,
                             @RequestBody GenerateDto request) {
        if (!language.equals("en-US")) {
            throw new ResponseStatusException(
                    HttpStatus.FORBIDDEN, "Bad HEADER."
            );
        }
        logger.info("Path: /user/{id}/generate. Method: POST. Id: \"" + id + "\". Query: \"" + request.getQuery() + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id) &&
            Pattern.matches("[a-zA-Z0-9]*", request.getQuery())) {
            return userService.create(id, request);
        } else {
            return "";
        }

    }

    @DeleteMapping("{id}/reset")
    public void resetCache(@PathVariable(name = "id") String id,
                           @RequestParam(name = "query", required = false, defaultValue = "") String query) {
        logger.info("Path: /user/{id}/reset. Method: DELETE. Id: \"" + id + "\". Query: \"" + query + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id) &&
            Pattern.matches("[a-zA-Z0-9]*", query)) {
            userService.resetCache(id, query);
        }
    }

    @DeleteMapping("{id}/clean")
    public void cleanAll(@PathVariable(name = "id") String id) {
        logger.info("Path: /user/{id}/cleanAll. Method: DELETE. Id: \"" + id + "\"");
        if (Pattern.matches("[a-zA-Z0-9]+", id)) {
            userService.cleanAll(id);
        }
    }
}
