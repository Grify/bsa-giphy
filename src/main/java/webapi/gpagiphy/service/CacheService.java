package webapi.gpagiphy.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import webapi.gpagiphy.dto.GifsByQuery;
import webapi.gpagiphy.dto.GiphyResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class CacheService {
    @Value(value = "${giphy.api-key}")
    private String API_KEY;
    @Value(value = "${giphy.get-data}")
    private String PATH_TO_GIPHY;
    @Value(value = "${giphy.download-ulr}")
    private String PATH_TO_DOWNLOAD;

    public List<GifsByQuery> getGifsByQuery(String query) {
        List<GifsByQuery> response = new ArrayList<>();
        new File("./src/main/resources/cache").mkdir();
        if (query.equals("")) {
            File catalog = new File("./src/main/resources/cache");
            for (File dir : Objects.requireNonNull(catalog.listFiles())) {
                ArrayList<String> fileNames = new ArrayList<>();
                for (File file : Objects.requireNonNull(dir.listFiles())) {
                    fileNames.add(file.getAbsolutePath());
                }
                GifsByQuery gifs = new GifsByQuery();
                gifs.setQuery(dir.getName());
                gifs.setGifs(fileNames);

                response.add(gifs);
            }
        } else {
            File dir = new File("./src/main/resources/cache/" + query);
            List<String> fileNames = new ArrayList<>();
            for (File file : Objects.requireNonNull(dir.listFiles())) {
                fileNames.add(file.getAbsolutePath());
            }
            GifsByQuery gifs = new GifsByQuery();
            gifs.setQuery(query);
            gifs.setGifs(fileNames);

            response.add(gifs);
        }

        return response;
    }

    public void delete() {
        new File("./src/main/resources/cache").mkdir();
        File catalog = new File("./src/main/resources/cache");
        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            for (File file : Objects.requireNonNull(dir.listFiles())) {
                file.delete();
            }
            dir.delete();
        }
    }

    public List<String> getAllGifs() {
        new File("./src/main/resources/cache").mkdir();
        File catalog = new File("./src/main/resources/cache");
        ArrayList<String> fileNames = new ArrayList<>();
        for (File dir : Objects.requireNonNull(catalog.listFiles())) {
            for (File file : Objects.requireNonNull(dir.listFiles())) {
                fileNames.add(file.getAbsolutePath());
            }
        }
        return fileNames;
    }

    public GifsByQuery create(String query) {
        RestTemplate restTemplate = new RestTemplate();
        GiphyResponse giphyResponse = restTemplate.getForObject(
                PATH_TO_GIPHY + "?api_key=" + API_KEY + "&q=" + query, GiphyResponse.class);
        if (giphyResponse == null) {
            throw new ResponseStatusException(
                    HttpStatus.BAD_GATEWAY, "Giphy does not work."
            );
        }
        String fileURL = PATH_TO_DOWNLOAD + giphyResponse.getData().get(0).getId() + ".gif";

        try {
            URL url = new URL(fileURL);
            ReadableByteChannel rbc = Channels.newChannel(url.openStream());
            new File("./src/main/resources/cache").mkdir();
            new File("./src/main/resources/cache/" + query).mkdir();
            FileOutputStream fos = new FileOutputStream(
                    "./src/main/resources/cache/" + query + "/" + giphyResponse.getData().get(0).getId() + ".gif");
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
            fos.close();
            rbc.close();
        } catch (IOException e ) {
            e.printStackTrace();
        }

        return this.getGifsByQuery(query).get(0);
    }
}

