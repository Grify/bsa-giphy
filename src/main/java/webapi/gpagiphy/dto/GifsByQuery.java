package webapi.gpagiphy.dto;

import lombok.Data;

import java.util.List;

@Data
public class GifsByQuery {
    private String query;
    private List<String> gifs;
}
